cp ./vagrant/Vagrantfile ../../
cp ./vagrant/000-default.conf ../../
cp ./vagrant/apache.php.ini ../../
cp ./vagrant/cli.php.ini ../../
cp ./vagrant/setup.sh ../../
cp ./conf/* ../../dev/config/
cd ../../
vagrant up
vagrant ssh -c "sudo chmod +x /var/www/html/setup.sh"
vagrant ssh -c "sudo /var/www/html/setup.sh"
cd ./tools
git clone git@bitbucket.org:twinpigs/oil-tools.git
chmod +x ./oil-tools/db-update/*.sh