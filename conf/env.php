<?php

ini_set('memory_limit', '456M');
setlocale(LC_ALL, 'ru', 'ru_RU.UTF-8');

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

//defined('YII_ENV_PROD') or define('YII_ENV_PROD', true);

$env = [
    'COOKIE_VALIDATION_KEY' => 'GKLcAp9cSLK1wJsU7Zga5_Uc8SHa97IO',
    'ADMIN_EMAIL'           => 'admin@example.com',
    'MAIL_FROM_MAIL'        => 'automessage@nikitapolunin.ru',
    'MAIL_FROM_TITLE'       => 'Eriell сайт',
    'VUE_DEV_SERVER_PORT'   => '8880',
    'VUE_DEV_SERVER_HOST'   => '127.0.0.1',
    'VUE_SKIP_CHECKING'     =>  true,
    'VUE_USE_DEV_SERVER'    =>  false,
];

foreach ($env as $name => $value) {
    putenv("{$name}={$value}");
}




