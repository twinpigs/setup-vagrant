<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=127.0.0.1;port=5432;dbname=eriell',
    'username' => 'eriell',
    'password' => 'eriell',
    'tablePrefix' => 'tbl_',
    'charset' => 'utf8',
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 3600,
];
