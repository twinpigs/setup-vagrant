<?php

$config = [
    'components' => [
        'wellSwitcher' => [
            'class' => 'app\components\wellSwitcher',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => $assets,
        ],
        'request' => [
            'cookieValidationKey' => getenv('COOKIE_VALIDATION_KEY'),
            'parsers' => [
                'application/json' => 'app\components\base\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Staff',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'app\components\EriellDbManager',
            'cache' => 'cache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.**********.***',
                'port' => '25',
                'username' => '**********',
                'password' => '**********',
            ],
        ],
        'log' => [
            'traceLevel' => 3,
            'targets' => [
                [
                    'class' => 'app\components\target\FileByDateTarget',
                    'maxLogFiles' => 10,
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class'          => 'yii\log\FileTarget',
                    'categories'     => ['excel'],
                    'logFile'        => '@app/runtime/logs/excel.log',
                    'exportInterval' => 1,
                    'levels'         => ['info','error','warning'],
                    'logVars'        => [],
                ],
                [
                    'class'          => 'yii\log\FileTarget',
                    'categories'     => ['vuejs_error'],
                    'logFile'        => '@app/runtime/logs/vue_errors.log',
                    'exportInterval' => 1,
                    'levels'         => ['error'],
                    'logVars'        => [],
                ],
                [
                    'class'          => 'app\components\LoginLogTarget',
                    'categories'     => ['user.login.success'],
                    'levels'         => ['info'],
                    'logVars'        => [],
                ],
                [
                    'class'          => 'app\components\SigninLogTarget',
                    'categories'     => ['api.signin.success'],
                    'levels'         => ['info'],
                    'logVars'        => [],
                ],
            ],
        ]
    ],
];

$config['params']['ldap'] = [
    'ldap_server' => 'ldap://ldap.jumpcloud.com',
    'ldap_port' => 389,
    'ldap_prefix' => 'uid=',
    'ldap_suffix' => ',ou=Users,o=5a9206eae5e346e02f1c387e,dc=jumpcloud,dc=com',
    'loginAttribute' => "uid",
    'loginSuffix' => '', //'@eriell.com' for prod
    'dn' => array(
        "ou=Users,o=5a9206eae5e346e02f1c387e,dc=jumpcloud,dc=com",
        "ou=Users,o=5a9206eae5e346e02f1c387e,dc=jumpcloud,dc=com"
    ),
    'attributesDbLdap' => [
        'firstName'=>'givenname',
        'lastName'=>'sn',
        'familyName'=>'middlename',
        'email'=>'mail',
        'phone'=>'phone',
        'employeenumber'=>'sn',
    ],
    'searchLdapAttributes' => [
        'sn',
        'givenname',
        'uid',
        'mail'
    ],
    'ldap_user'=> [
        'login'=> 'rbm.test01,ou=Users,o=5a9206eae5e346e02f1c387e,dc=jumpcloud,dc=com',
        'password' => 'pass',
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*.*.*.*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'default' => '@app/components/gii/templates/crud/default',
                ]
            ]
        ],
        'allowedIPs' => ['*.*.*.*'],
    ];
}

return $config;
