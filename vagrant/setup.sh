apt-get update
apt-get install -y apache2 apache2-utils composer zip unzip php php-zip php-mbstring php-xml php-cli php-pgsql php-ldap postgresql postgresql-client postgresql-client-common postgresql-contrib curl php-curl
a2enmod rewrite
cp /var/www/html/000-default.conf /etc/apache2/sites-available/
cp /var/www/html/apache.php.ini /etc/php/7.4/apache2/php.ini
cp /var/www/html/cli.php.ini /etc/php/7.4/cli/php.ini
service apache2 restart
sudo -u postgres createuser eriell
sudo -u postgres createuser readonly
#sudo -u postgres psql postgres #to set password 'eriell' for eriell
cd /var/www/html/dev
composer install
rm /var/www/html/000-default.conf
rm /var/www/html/apache.php.ini
rm /var/www/html/cli.php.ini
rm /var/www/html/index.html
rm /var/www/html/setup.sh